﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	public LevelTrack levelTrack; 

    public float speed;
    public Text countText;
	public Text totalCountText;
    public AudioClip collectSound;
    public AudioClip lavaSound;
    public int normalCount;
    public int specialCount;

	public GameObject collisionEffect;
	public GameObject collisionRed;

    private Rigidbody2D rb2d;
    private int count;
	List<int> pickupCount = new List<int>();


	// Use this for initialization
	void Start () {
        rb2d = GetComponent<Rigidbody2D>();
        count = 0;
        SetCountText();
		pickupCount.Add (12);
		pickupCount.Add (8);
    }
		
	
	// Update is called once per frame
	void FixedUpdate () {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector2 movement = new Vector2(moveHorizontal, moveVertical);
        rb2d.AddForce(movement * speed);
    }

    void OnTriggerEnter2D(Collider2D other) {
        if (other.gameObject.CompareTag("PickUp") || other.gameObject.CompareTag("Pickup2")) {
            if (other.gameObject.CompareTag("PickUp")) {
				Instantiate(collisionEffect, other.transform.position, other.transform.rotation);
                AudioSource.PlayClipAtPoint(collectSound, transform.position);
            } else {
				Instantiate(collisionRed, other.transform.position, other.transform.rotation);
                AudioSource.PlayClipAtPoint(lavaSound, transform.position);
            }

			count += 1;
			LevelTrack.totalScore += 1;

            other.gameObject.SetActive(false);
            SetCountText();

			checkCount ();
        }
    }

    void SetCountText() {
        countText.text = "Level Count: " + count.ToString ();
		totalCountText.text = "Total Count: " + LevelTrack.totalScore.ToString ();
    }

	void checkCount() {
		if ((count == pickupCount [LevelTrack.currentLevel-1]) && (LevelTrack.currentLevel + 1 > LevelTrack.numberLevels)) { //currentLevel starts at 1 but the first value in the list is at index 0
			SceneManager.LoadScene ("ReplayWin");
		} else if (count == pickupCount [LevelTrack.currentLevel - 1]) {
			LevelTrack.currentLevel += 1;
			SceneManager.LoadScene ("Level" + LevelTrack.currentLevel.ToString ());
		}
	}
}
