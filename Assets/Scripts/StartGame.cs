﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartGame : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	public void OnGUI () {
		GUI.contentColor = Color.yellow;
		if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 3, 100, 40), "Play")) {
			LevelTrack.currentLevel = 1;
			LevelTrack.totalScore = 0;
			SceneManager.LoadScene ("Level1");
		}
	}
}
