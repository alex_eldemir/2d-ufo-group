﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ReplayScript : MonoBehaviour {
	

	// Use this for initialization
	void Start () {
		
	}

	public void OnGUI () {
		GUI.contentColor = Color.yellow;
		if (GUI.Button (new Rect (Screen.width / 2 - 50, Screen.height / 3, 100, 40), "Play Again?")) {
			LevelTrack.currentLevel = 1;
			LevelTrack.totalScore = 0;
			SceneManager.LoadScene ("MainMenu");
		} 
	}
		
}
